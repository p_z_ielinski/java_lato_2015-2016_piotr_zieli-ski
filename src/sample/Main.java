package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
        import javafx.scene.Parent;
        import javafx.scene.Scene;
        import javafx.stage.Stage;
import sample.Models.Client;


public class Main extends Application {
    public Main() {
    }


    public void start(Stage primaryStage) throws Exception {
        try {
            Parent e = (Parent)FXMLLoader.load(this.getClass().getResource("view/Main.fxml"));
            Scene scene = new Scene(e);
            scene.getStylesheets().add(this.getClass().getResource("Styles/main.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (Exception var4) {
            var4.printStackTrace();
        }

    }


    public static void main(String[] args) {
        launch(args);
    }
}
