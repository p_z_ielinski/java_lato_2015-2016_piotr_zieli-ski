package sample.Data;

import java.sql.Connection;
import java.sql.DriverManager;

public class SqliteConnection {
    public SqliteConnection() {
    }

    public static Connection Connector() {
        try {
            Class.forName("org.sqlite.JDBC");
            return DriverManager.getConnection("jdbc:sqlite:UsersDB.sqlite");

        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
    }
}
