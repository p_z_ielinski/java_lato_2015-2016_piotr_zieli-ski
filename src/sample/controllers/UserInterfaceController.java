package sample.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import sample.Models.Boulder;
import sample.Models.RankingData;
import sample.Models.Repositories;

public class UserInterfaceController implements Initializable{

    @FXML
    private Button logOutBtn;

    public Label getUserName() {
        return userName;
    }

    public void setUserName(Label userName) {
        this.userName = userName;
    }

    @FXML
    private Label userName;

    public TableView getTableBoulders() {
        return tableBoulders;
    }

    public ObservableList<Boulder> getData() {
        return data;
    }


    @FXML
    private TableView<Boulder> tableBoulders;

    private ObservableList<Boulder> data = FXCollections.observableArrayList();

    TableColumn idColumn = new TableColumn("ID");
    TableColumn loginColumn = new TableColumn("IMIĘ");
    TableColumn regionColumn = new TableColumn("REGION");
    TableColumn sectorColumn = new TableColumn("SECTOR");
    TableColumn gradeColumn = new TableColumn("WYCENA");
    TableColumn styleColumn = new TableColumn("STYL");

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        idColumn.setMinWidth(106.666);
        idColumn.setCellValueFactory(
                new PropertyValueFactory<RankingData,String>("id"));

        loginColumn.setMinWidth(106.666);
        loginColumn.setCellValueFactory(
                new PropertyValueFactory<RankingData,String>("name")
        );

        regionColumn.setMinWidth(106.666);
        regionColumn.setCellValueFactory(
                new PropertyValueFactory<RankingData,String>("region")
        );

        sectorColumn.setMinWidth(106.666);
        sectorColumn.setCellValueFactory(
                new PropertyValueFactory<RankingData,String>("sector")
        );

        gradeColumn.setMinWidth(106.666);
        gradeColumn.setCellValueFactory(
                new PropertyValueFactory<RankingData,String>("grade")
        );

        styleColumn.setMinWidth(106.666);
        styleColumn.setCellValueFactory(
                new PropertyValueFactory<RankingData,String>("style")
        );

        tableBoulders.getColumns().addAll(idColumn, loginColumn, regionColumn, sectorColumn, gradeColumn, styleColumn);

    }

    public void GetUser(String user) {
        userName.setText(user);
    }
    public void SignOut(javafx.event.ActionEvent event) throws InterruptedException {

        try {
            ((Node)event.getSource()).getScene().getWindow().hide();

            Stage primaryStage = new Stage();
            FXMLLoader loader = new FXMLLoader();

            Pane root = loader.load(getClass().getResource("../view/tab/Menu.fxml").openStream());
            Scene scene = new Scene(root);
            scene.getStylesheets().add(getClass().getResource("../Styles/main.css").toExternalForm());

            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
    public void newBoulderWindow(javafx.event.ActionEvent event) throws InterruptedException {
        try {

            Stage primaryStage = new Stage();
            FXMLLoader loader = new FXMLLoader();

            Pane root = loader.load(getClass().getResource("../view/tab/NewBoulder.fxml").openStream());
            Scene scene = new Scene(root);
            scene.getStylesheets().add(getClass().getResource("../Styles/main.css").toExternalForm());

            primaryStage.setScene(scene);
            primaryStage.show();


            //to mi tworzy instancje newBoulderController przy otwierzaniu okna i ustawiam nazwe uzytkownika
            //na text w labelu zeby potem go pobrac przy tworzeniu nowej tabeli z boulderami
           NewBoulderController newBoulderController = (NewBoulderController)loader.getController();
           newBoulderController.setUserNameLbl(userName.getText());


        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}

