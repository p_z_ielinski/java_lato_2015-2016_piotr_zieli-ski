package sample.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import sample.Models.Repositories;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class NewBoulderController implements Initializable {

    private MainController main;
    public Repositories repositories = new Repositories();

    public void setUserNameLbl(String userNameLbl) {
        this.userNameLbl.setText(userNameLbl);
    }

    ObservableList<String> gradeList = FXCollections.observableArrayList(
            "6A","6A+","6B","6B+","6C","6C+","7A","7A+","7B","7B+","7C","7C+","8A","8A+","8B","8B+","8C","8C+");
    ObservableList<String> styleList = FXCollections.observableArrayList("Flash","RP");

    @FXML
    private Label userNameLbl;

    @FXML
    private Button newBoulderBtn;

    @FXML
    private DatePicker boulderDate;

    @FXML
    private TextField boulderName;

/*    @FXML
    private TextField grade;

    @FXML
    private TextField style;*/

    @FXML
    private ChoiceBox gradeBox;

    @FXML
    private ChoiceBox styleBox;

    @FXML
    private GridPane newAccount;

    @FXML
    private TextField region;

    @FXML
    private TextField sector;

    public void addNewBoulder(ActionEvent event) throws InterruptedException, IOException {
        FXMLLoader loader = new FXMLLoader();

        //czemu to nie dziala?!?!?
       ((Node)event.getSource()).getScene().getWindow().hide();

        String userName = userNameLbl.getText();

        //uruchamiam funkcje dodajaca
        repositories.addBoulder(
                userName,
                boulderName.getText(),
                region.getText(),
                sector.getText(),
                gradeBox.getValue().toString(),
                styleBox.getValue().toString()
                );

        Stage primaryStage = new Stage();
        Pane root = loader.load(getClass().getResource("../view/tab/UserInterface.fxml").openStream());
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.show();
        scene.getStylesheets().add(getClass().getResource("../Styles/main.css").toExternalForm());
        UserInterfaceController userInterfaceController = loader.getController();


        repositories.buildUserData(userName,userInterfaceController.getTableBoulders(),userInterfaceController.getData());

        ((Node)event.getSource()).getScene().getWindow().hide();

    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        styleBox.setItems(styleList);
        gradeBox.setItems(gradeList);
    }
}
