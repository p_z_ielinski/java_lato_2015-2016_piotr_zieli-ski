package sample.controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import sample.Models.Repositories;

import java.io.IOException;

public class NewAccountController {
    private MainController main;

    public Repositories repositories = new Repositories();

    @FXML
    private TextField userPassword;
    @FXML
    private TextField userLastName;
    @FXML
    private TextField userSpecific;
    @FXML
    private TextField userRepeatPassword;
    @FXML
    private TextField userFirstName;
    @FXML
    private Button createAccount;
    @FXML
    private DatePicker userDate;



    public boolean equals(Object o) {
        if(this == o) {
            return true;
        } else if(!(o instanceof NewAccountController)) {
            return false;
        } else {
            NewAccountController that = (NewAccountController)o;
            if(this.userPassword != null) {
                if(this.userPassword.equals(that.userPassword)) {
                    return this.userRepeatPassword != null?this.userRepeatPassword.equals(that.userRepeatPassword):that.userRepeatPassword == null;
                }
            } else if(that.userPassword == null) {
                return this.userRepeatPassword != null?this.userRepeatPassword.equals(that.userRepeatPassword):that.userRepeatPassword == null;
            }

            return false;
        }
    }

    public void newAccount(javafx.event.ActionEvent event) throws InterruptedException, IOException {


        if( userPassword.getText().equals( userRepeatPassword.getText()) && !userSpecific.getText().equals("") ){

            repositories.addUser(userFirstName.getText(),userLastName.getText(),userPassword.getText(),userSpecific.getText());

            try {

                Stage primaryStage = new Stage();
                FXMLLoader loader = new FXMLLoader();

                Pane root = loader.load(getClass().getResource("../view/tab/NewBoulder.fxml").openStream());
                Scene scene = new Scene(root);
                scene.getStylesheets().add(getClass().getResource("../Styles/main.css").toExternalForm());

                primaryStage.setScene(scene);
                primaryStage.show();

                NewBoulderController newBoulderController = (NewBoulderController)loader.getController();
                newBoulderController.setUserNameLbl(userSpecific.getText());


            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            Stage primaryStage = new Stage();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/tab/NewAccountError.fxml"));
            Parent root = (Parent)loader.load();

            primaryStage.setScene(new Scene(root));
            primaryStage.initModality(Modality.APPLICATION_MODAL);
            primaryStage.showAndWait();
        }
    }


    public void backToMenu(javafx.event.ActionEvent event) throws InterruptedException{
        Stage primaryStage = new Stage();
        FXMLLoader loader = new FXMLLoader();

        try {
            ((Node)event.getSource()).getScene().getWindow().hide();

            Pane root = loader.load(getClass().getResource("../view/tab/Menu.fxml").openStream());
            Scene scene = new Scene(root);
            scene.getStylesheets().add(getClass().getResource("../Styles/main.css").toExternalForm());

            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
