package sample.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;

import java.net.URL;
import java.util.ResourceBundle;

public class MainController implements Initializable {
    @FXML
    MenuController menuController;

    @FXML
    public void initialize() {
        menuController.init(this);
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
