package sample.controllers;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import sample.Models.Repositories;

import java.io.IOException;

/**
 * Created by Piotr on 2016-05-14.
 */
public class MenuController {


    private MainController main;


    @FXML
    private Button loginMain;

    @FXML
    private Button exit;

    @FXML
    private Button showRanking;

    @FXML
    private Button newAccount;



    public void tabHandler(ActionEvent event){
        Stage primaryStage = new Stage();
        FXMLLoader loader = new FXMLLoader();

        if(event.getSource() == loginMain){
            try {

                ((Node)event.getSource()).getScene().getWindow().hide();

                Pane root = loader.load(getClass().getResource("../view/tab/Login.fxml").openStream());
                Scene scene = new Scene(root);
                scene.getStylesheets().add(getClass().getResource("../Styles/main.css").toExternalForm());

                primaryStage.setScene(scene);
                primaryStage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else if(event.getSource() == newAccount){
            try {
                ((Node)event.getSource()).getScene().getWindow().hide();

                Pane root = loader.load(getClass().getResource("../view/tab/NewAccount.fxml").openStream());
                Scene scene = new Scene(root);
                scene.getStylesheets().add(getClass().getResource("../Styles/main.css").toExternalForm());

                primaryStage.setScene(scene);
                primaryStage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else if(event.getSource() == showRanking){
            try {
                ((Node)event.getSource()).getScene().getWindow().hide();

                Pane root = loader.load(getClass().getResource("../view/tab/Ranking.fxml").openStream());
                Scene scene = new Scene(root);
                scene.getStylesheets().add(getClass().getResource("../Styles/main.css").toExternalForm());

                primaryStage.setScene(scene);
                primaryStage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }else if(event.getSource() == exit){
            Platform.exit();
        }
    }


    public void init(MainController mainController) {
        main = mainController;
    }

}
