package sample.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import sample.Models.RankingData;
import sample.Models.Repositories;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Piotr on 2016-05-29.
 */
public class RankingController implements Initializable {

    public Repositories repositories = new Repositories();

    @FXML
    private TableView<RankingData> tableRanking;

    private ObservableList<RankingData> data = FXCollections.observableArrayList();
    TableColumn idColumn = new TableColumn("ID");
    TableColumn loginColumn = new TableColumn("LOGIN");
    TableColumn pointsColumn = new TableColumn("PUNKTY");

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        repositories.createRanking();

        idColumn.setMinWidth(106.666);
        idColumn.setCellValueFactory(
                new PropertyValueFactory<RankingData,String>("id"));

        loginColumn.setMinWidth(106.666);
        loginColumn.setCellValueFactory(
                new PropertyValueFactory<RankingData,String>("login")
        );

        pointsColumn.setMinWidth(106.666);
        pointsColumn.setCellValueFactory(
                new PropertyValueFactory<RankingData,String>("points")
        );

        repositories.buildRankingData(tableRanking,data);
        tableRanking.getColumns().addAll(idColumn, loginColumn, pointsColumn);

    }

    public void backToMenu(javafx.event.ActionEvent event) throws InterruptedException{
        Stage primaryStage = new Stage();
        FXMLLoader loader = new FXMLLoader();

        try {
            ((Node)event.getSource()).getScene().getWindow().hide();

            Pane root = loader.load(getClass().getResource("../view/tab/Menu.fxml").openStream());
            Scene scene = new Scene(root);
            scene.getStylesheets().add(getClass().getResource("../Styles/main.css").toExternalForm());

            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
