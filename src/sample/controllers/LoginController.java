package sample.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;

import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import sample.Models.Repositories;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;


public class LoginController implements Initializable {

    public Repositories repositories = new Repositories();

    @FXML
    private Label isConnected;

    @FXML
    private Button loginUser;

    @FXML
    private PasswordField insertPassword;

    @FXML
    private TextField insertLogin;

    public void Login(ActionEvent event) throws InterruptedException {

        try {
            if(repositories.isLogin(insertLogin.getText(),insertPassword.getText())){
                isConnected.setText("login and password correct");

                ///////////////////////////////////
                ((Node)event.getSource()).getScene().getWindow().hide();

                Stage primaryStage = new Stage();
                FXMLLoader loader = new FXMLLoader();
                Pane root = loader.load(getClass().getResource("../view/tab/UserInterface.fxml").openStream());

                Scene scene = new Scene(root);
                scene.getStylesheets().add(getClass().getResource("../Styles/main.css").toExternalForm());
                primaryStage.setScene(scene);
                primaryStage.show();
                /////////////////////////////////////


                //to mi tworzy instancje userController przy otwierzaniu okna
                UserInterfaceController userInterfaceController = (UserInterfaceController)loader.getController();

                //to ustawia nazwe uzytkownika do labela
                userInterfaceController.GetUser(insertLogin.getText());
                repositories.buildUserData(insertLogin.getText(),userInterfaceController.getTableBoulders(),userInterfaceController.getData());

            }else {
                isConnected.setText("login or password not correct");
            }
        } catch (SQLException e) {
            isConnected.setText("not correct");
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void backToMenu(javafx.event.ActionEvent event) throws InterruptedException{
        Stage primaryStage = new Stage();
        FXMLLoader loader = new FXMLLoader();

        try {
            ((Node)event.getSource()).getScene().getWindow().hide();

            Pane root = loader.load(getClass().getResource("../view/tab/Menu.fxml").openStream());
            Scene scene = new Scene(root);
            scene.getStylesheets().add(getClass().getResource("../Styles/main.css").toExternalForm());

            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        if(repositories.isDbConnected()){
            isConnected.setText("connected");
        }else {
            isConnected.setText("not connected");
        }

    }
}

