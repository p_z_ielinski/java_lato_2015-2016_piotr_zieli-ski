package sample.Models;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.XYChart;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import sample.Data.SqliteConnection;

import java.sql.*;

import java.sql.Connection;
import java.util.Comparator;
import java.util.stream.Collectors;

import static java.util.Comparator.comparing;

public class Repositories {
    Connection connection;


    //polaczenie
    public Repositories(){
        connection = SqliteConnection.Connector();
        if(connection == null) {
            System.out.println("nie polaczono");
            System.exit(1);//jak sie nie polaczy z baza to wywala aplikacje
        }
    }
    public boolean isDbConnected(){
        try {
            return !connection.isClosed();
        } catch (SQLException e) {
            System.out.println("Blad!!!: "+ e);
            e.printStackTrace();
            return false;
        }
    }

    //sprawdzam czy zalogowany
    public boolean isLogin(String user, String pass) throws SQLException {

        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        String query = "select * from user where login = ? and password = ?";

        try{
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1,user);
            preparedStatement.setString(2,pass);

            resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                return true;
            }else {
                return false;
            }
        }catch (Exception e){
            e.printStackTrace();
            return false;
        } finally {
            preparedStatement.close();
            resultSet.close();
        }

    }

    public void usersNumber(String user) throws SQLException {

        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        String query = "select * from usersCount where uCount = ? ";

        try{
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1,user);

            resultSet = preparedStatement.executeQuery();


        }catch (Exception e){
            e.printStackTrace();
        }

    }


    //dodaje uzytkownika
    public void addUser(String firstName, String lastName, String password, String login){

        try {
            String query = "insert into user (firstName, lastName, password, login) values (?,?,?,?)";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, firstName);
            preparedStatement.setString(2, lastName);
            preparedStatement.setString(3, password);
            preparedStatement.setString(4, login);

            preparedStatement.execute();

            System.out.println("dodano uzytkownika!!!");

            preparedStatement.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        int initRanking = 0;

        // to inicjalizuje tabele dla danego uzytkownika w rankingu zebym potem mogl juz tylko updatowac ze nie musial jej tworzyc
        try {
            String query = "insert into ranking (login,points) values (?,?)";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, login);
            preparedStatement.setInt(2, initRanking);


            preparedStatement.execute();

            preparedStatement.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addBoulder(String userName, String boulderName, String region, String sector, String grade, String style){


        try{
            String query = "CREATE TABLE IF NOT EXISTS " + userName +"_boulders (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , bouldername TEXT, region TEXT, sector TEXT, grade TEXT, style TEXT)";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.executeUpdate();

            preparedStatement.close();

        }catch  (SQLException e) {
            e.printStackTrace();
        }

        try {
            String query = "insert into " + userName +"_boulders (boulderName, region, sector, grade, style) values (?,?,?,?,?)";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, boulderName);
            preparedStatement.setString(2, region);
            preparedStatement.setString(3, sector);
            preparedStatement.setString(4, grade);
            preparedStatement.setString(5, style);

            preparedStatement.execute();

            System.out.println("dodano boulder!!!");

            preparedStatement.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

    public void buildUserData(String userName, TableView<Boulder> tableview,ObservableList<Boulder> data){

        try {
            String query = "SELECT * from " + userName +"_boulders";
            ResultSet resultSet = connection.createStatement().executeQuery(query);

            while (resultSet.next()){
                ObservableList<String> row = FXCollections.observableArrayList();

                for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++) {
                    row.add(resultSet.getString(i));
                }
                data.add(new Boulder(row.get(0).toString(),row.get(1).toString(),row.get(2).toString(),row.get(3).toString(),row.get(4).toString(),row.get(5).toString()));
            }
            tableview.setItems(data);

            System.out.println("dodano buldery!!!");
            resultSet.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void buildRankingData(TableView<RankingData> tableview,ObservableList<RankingData> data){



        try {
            String query = "SELECT * from ranking";
            ResultSet resultSet = connection.createStatement().executeQuery(query);

            while (resultSet.next()) {
                ObservableList<String> row = FXCollections.observableArrayList();

                for (int i = 1; resultSet.getMetaData().getColumnCount() >= i; i++) {
                    row.add(resultSet.getString(i));
                }
                data.add(new RankingData(row.get(0).toString(),row.get(1).toString(),row.get(2).toString()));
            }

            tableview.setItems(data);

            System.out.println("uzupelniono ranking");
            resultSet.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }



    public ObservableList<String> getUserLogins(){
        ObservableList<String> login = FXCollections.observableArrayList();
        try {
            String query = "SELECT login FROM user";
            ResultSet resultSet = connection.createStatement().executeQuery(query);

            while(resultSet.next()){
                for (int i = 1; resultSet.getMetaData().getColumnCount() >= i; i++) {
                    login.add(resultSet.getString(i));
                }
            }

        }catch (SQLException e) {
            e.printStackTrace();
        }

        return login;
    }

    public int countPoints(ObservableList<String> login,int points, int i, String reg6,String reg7,String reg8,String regA,String regB,String regC,String regPlus){
        //czy dobrze pobiera przejscia
        //ObservableList<String> grades = FXCollections.observableArrayList();
            try {
                String currentUserLogin = login.get(i).toString();

                String query = "SELECT grade, style from " + currentUserLogin +"_boulders";
                ResultSet resultSet = connection.createStatement().executeQuery(query);

                while(resultSet.next()){
                    for (int j = 1; resultSet.getMetaData().getColumnCount() >= j; j++) {

                        int tmpPoints = 0;

                        if(resultSet.getString(j).matches(reg6)){
                            tmpPoints += 50;
                        }else if (resultSet.getString(j).matches(reg7)){
                            tmpPoints += 200;
                        }else if (resultSet.getString(j).matches(reg8)){
                            tmpPoints += 1000;
                        }

                        if (resultSet.getString(j).matches(regA)){
                            tmpPoints *= 1.5;
                        }else if (resultSet.getString(j).matches(regB)){
                            tmpPoints *= 2;
                        }else if (resultSet.getString(j).matches(regC)){
                            tmpPoints *= 2.5;
                        }

                        if (resultSet.getString(j).matches(regPlus)){
                            tmpPoints *= 1.5;
                        }

                        //to mi sprawdza czy ktores przejscie jest flashem
                        if( (resultSet.getString(j).matches(reg6) || resultSet.getString(j).matches(reg7) || resultSet.getString(j).matches(reg8)) && resultSet.getString(j+1).matches("Flash")   ){
                            tmpPoints += 4;
                        }
                        points += tmpPoints;

                        //grades.add(resultSet.getString(j));
                    }
                }
               // System.out.println(grades);

            }catch (SQLException e) {
                e.printStackTrace();
            }

        return points;
    }

    public void createRanking(){
        ObservableList<String> login = getUserLogins();
        int userCount = login.size();
        int points;
        String reg6 = "6.+";
        String reg7 = "7.+";
        String reg8 = "8.+";
        String regA = ".A.*";
        String regB = ".B.*";
        String regC = ".C.*";
        String regPlus = ".*"+"\\+";

        for (int i = 0; i <userCount; i++) {
            //zeruje pkt zeby kazdy uzytkownik zaczynal od 0
            points =0;
            try {
                String currentUserLogin = login.get(i).toString();
                points = countPoints(login,points,i,reg6,reg7,reg8,regA,regB,regC,regPlus);

                String query = "UPDATE ranking SET points = ? Where login = ?";
                PreparedStatement preparedStatement = connection.prepareStatement(query);
                preparedStatement.setInt(1, points);
                preparedStatement.setString(2, currentUserLogin);

                preparedStatement.executeUpdate();
                preparedStatement.close();

                System.out.println("ranking gotowy");

            }catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

}

