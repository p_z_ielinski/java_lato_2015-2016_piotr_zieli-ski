package sample.Models;

import javafx.beans.property.SimpleStringProperty;

public class RankingData {

    private final SimpleStringProperty id;
    private final SimpleStringProperty login;
    private final SimpleStringProperty points;


    public String getPoints() {
        return points.get();
    }

    public SimpleStringProperty pointsProperty() {
        return points;
    }

    public void setPoints(String points) {
        this.points.set(points);
    }

    public String getLogin() {
        return login.get();
    }

    public SimpleStringProperty loginProperty() {
        return login;
    }

    public void setLogin(String login) {
        this.login.set(login);
    }

    public String getId() {
        return id.get();
    }

    public SimpleStringProperty idProperty() {
        return id;
    }

    public void setId(String id) {
        this.id.set(id);
    }



    public RankingData(String fName, String lName, String email) {
        this.id = new SimpleStringProperty(fName);
        this.login = new SimpleStringProperty(lName);
        this.points = new SimpleStringProperty(email);
    }




}

