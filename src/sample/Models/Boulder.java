package sample.Models;

import javafx.beans.property.SimpleStringProperty;

import java.util.Date;

public class Boulder {
    private final SimpleStringProperty id;
    private final SimpleStringProperty name;
    private final SimpleStringProperty region;
    private final SimpleStringProperty sector;
    private final SimpleStringProperty grade;
    private final SimpleStringProperty style;


    public String getId() {
        return id.get();
    }

    public SimpleStringProperty idProperty() {
        return id;
    }

    public void setId(String id) {
        this.id.set(id);
    }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getRegion() {
        return region.get();
    }

    public SimpleStringProperty regionProperty() {
        return region;
    }

    public void setRegion(String region) {
        this.region.set(region);
    }

    public String getSector() {
        return sector.get();
    }

    public SimpleStringProperty sectorProperty() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector.set(sector);
    }

    public String getGrade() {
        return grade.get();
    }

    public SimpleStringProperty gradeProperty() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade.set(grade);
    }

    public String getStyle() {
        return style.get();
    }

    public SimpleStringProperty styleProperty() {
        return style;
    }

    public void setStyle(String style) {
        this.style.set(style);
    }




    public Boulder(String id, String name, String region, String sector, String grade, String style) {
        this.id = new SimpleStringProperty(id);
        this.name = new SimpleStringProperty(name);
        this.region = new SimpleStringProperty(region);
        this.sector = new SimpleStringProperty(sector);
        this.grade = new SimpleStringProperty(grade);
        this.style = new SimpleStringProperty(style);
    }
}
