package sample;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import sample.Models.Client;
import sample.Models.Server;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Piotr on 2016-06-08.
 */
public class Chat extends Application {

    private TextArea messages = new TextArea();

    private  boolean isServer = false;


    public NetworkConnection connection = isServer ? createServer() : createClient();



    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setScene(new Scene(createContent()));
        primaryStage.show();
    }

    private Parent createContent() {
        messages.setPrefHeight(550);
        TextField input = new TextField();
        input.setOnAction(event -> {
            String message = isServer ? "Server: " : "Client: ";
            message += input.getText();
            input.clear();

            messages.appendText(message+ "\n");

            try {
                connection.send(message);
            } catch (Exception e) {
               System.out.println("nie udało sie wyslac");
            }
        });


        VBox root = new VBox(20, messages, input);
        root.setPrefSize(500,500);

        return root;
    }


    private Client createClient() {
        System.out.println("teraz client");
        return new Client("127.0.0.1", 55555, data -> {
            Platform.runLater(() -> {
                messages.appendText(data.toString() + "\n");
            });
        });
    }
    private Server createServer() {
        System.out.println("server odpalonyyy!");
        return new Server(55555, data -> {
            Platform.runLater(() -> {
                messages.appendText(data.toString() + "\n");
            });
        }
        );
    }

    @Override
    public void stop() throws Exception {
        connection.closeConnection();
    }


    @Override
    public void init() throws Exception {
        connection.startConnection();
    }
}
